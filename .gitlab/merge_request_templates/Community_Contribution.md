### Contributions to the GitLab Project

* [ ]  By contributing to the GitLab project, I agree to the following where applicable:

- contributions made on behalf of an individual to the `ee/` directory are subject to the [Individual Contributor License Agreement](https://docs.gitlab.com/ee/legal/individual_contributor_license_agreement.html)

- contributions made on behalf of a company to the `ee/` directory are subject to the [Corporate Contributor License Agreement](https://docs.gitlab.com/ee/legal/individual_contributor_license_agreement.html)

- contributions made **outside** the `ee/` directory are subject to the [Developer Certificate of Origin](https://docs.gitlab.com/ce/legal/developer_certificate_of_origin.html#developer-certificate-of-origin-version-11)

~'Community Contribution'
